/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtarasiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 16:25:43 by vtarasiu          #+#    #+#             */
/*   Updated: 2018/04/03 18:30:38 by vtarasiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "libft/libft.h"
#include "fillit.h"

void	throw_error()
{
	write(1, "error\n", 6);
	exit(1);
}

t_tetr	*create_tetrimino(const char *field, const char letter)
{
	int 	i;
	int 	j;
	int 	x;
	int 	y;
	t_tetr	*new;

	new = (t_tetr *)malloc(sizeof(t_tetr));
	if (new == NULL || field == NULL || (field[0] != '.' && field[0] != '#'))
		throw_error();
	i = 0;
	j = 0;
	while (i < 25)
	{
		if (field[i] == '.' || field[i] == '\n')
			continue;
		else if (field[i] == '#')
		{
			new->letter = letter;
			new->scheme[j].x =
		}
		else
			throw_error();
		i++;
	}
	return (new);
}

void	read_tetriminos(char *file)
{
	int			fd;
	char 		letter;
	char		buffer[26];
	long long	bytes;

	fd = open(file, O_RDONLY);
	letter = 'A';
	if (fd == -1)
		return ;
	while ((bytes = read(fd, buffer, 26)) > 0)
		if ((bytes == 25 || bytes == 26) && letter + 1 <= 'Z')
			create_tetrimino(buffer, letter++);
		else
			throw_error();
	close(fd);
}

int		main(int argc, char **argv)
{
	read_tetriminos(argv[1]);
	return 0;
}