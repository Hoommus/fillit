/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtarasiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 18:09:00 by vtarasiu          #+#    #+#             */
/*   Updated: 2018/04/03 17:59:33 by vtarasiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_FILLIT_H
# define FILLIT_FILLIT_H



typedef struct	s_point
{
	int 	x;
	int 	y;
}				t_point;

typedef struct	s_tetrimino
{
	char 				letter;
	//struct s_point		*scheme;
	struct s_point		scheme[4];
}				t_tetr;

#endif
